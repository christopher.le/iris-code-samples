"""
Google Search will use the Google Search API to query Google and return the
top result.
"""

from langchain.tools import Tool
from langchain.utilities import GoogleSearchAPIWrapper
import os


# These key belongs to Accenture and Project Iris

# Google API Key
# Go here to get the API key:
# https://console.cloud.google.com/apis/credentials/key/0cdd9cfd-f5f5-486a-a8c9-f014e4833196?project=acn-agbg-ai
GOOGLE_API_KEY = ""

# Google custom search engine ID
# https://programmablesearchengine.google.com/controlpanel/overview?cx=2786bd50b2d1f49e8
GOOGLE_CSE_ID = "2786bd50b2d1f49e8"


def google_search():
    """Return a langchain tool that can do a Google Search."""

    os.environ["GOOGLE_API_KEY"] = GOOGLE_API_KEY
    os.environ["GOOGLE_CSE_ID"] = GOOGLE_CSE_ID

    search = GoogleSearchAPIWrapper()

    return Tool(
        name="Google Search",
        description="Useful for when you need to answer questions about current events or the current state of the world.",
        func=search.run
    )