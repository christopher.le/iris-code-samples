#!/bin/bash

# Google SDKs
pip install google-cloud-aiplatform
pip install google-auth
pip install google-api-python-client

# Prompt Toolkit is used to create a command line CLI.
pip install prompt_toolkit

# Make pretty colors on the command line.
pip install colorama