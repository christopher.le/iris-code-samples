from langchain import LLMMathChain
from langchain.agents.tools import Tool
from llm.vertex_ai import google_bard

def calculator():
    llm = google_bard()
    llm_math_chain = LLMMathChain.from_llm(llm=llm, verbose=True)
    return Tool(
        name="Calculator",
        func=llm_math_chain.run,
        description="Useful for when you need to answer questions about math"
    )