# Langchain with Tools Example

Example of the AI talking to itself using a agent chain.

## How it works

Basically, it creates a prompt then takes the result of that prompt and asks
itself, does this answer the question. If not, it adds that info the it's
"scratch pad", and asks itself, "With all the information I've gathered
what do I need to do?". It loops over and over until it can answer the
original question.

At every turn it generates a prompt string which concats:
- Decribe the personality of the AI: (Ie: "You are an assistant.")
- List of tools it can use.
- Specific instructions on output (a JSON string)
- What were last results were from the loop.

The output is JSON with the keys `action` and `action_input`. The actions
map to a function and `action_input` maps to the function's argument. Tools
expect a single string in, and expected to output a string. The output is
put back into the next prompt string.

Known issues:
- It might stuck in a loop.
- It might misunderstand your instructions.
- It might misunderstand it's own instructions or results from tools.

Further work:
- Adding another history memory to keep memory between user-to-AI, not just
  AI-to-AI.
- Creating customized tools to give the AI ability to perform specific tasks.
  (See CVE Database as an example)


------------------------------------------------------------------------------

## Setup:

- Run `./setup.sh`
- Run `./run_local.sh`

------------------------------------------------------------------------------

## Example Output:

```
$ ./run_local.sh

Getting Google credentials. Please wait ...
Client ID = 764086051850-6qr4p6gpi6hn506pt8ejuq83di341hur.apps.googleusercontent.com


LANGCHAIN WITH TOOLS EXAMPLE
----------------------------

How it works:
  - This color is what is sent to Vertex AI as the prompt.
  - This color is what is raw output of Vertex AI.

Instructions:
  - Up/down: Scroll through your previous history.
  - Left/right/delete/backspace: Edit your input.
  - Type "!Q" or Control-C to quit this program.

Try these:
  What is the current new about the Ukraine war?
  What files are in my current directory?
  What is the number of files in my current directory multiplied by the number of seasons Babe Ruth played baseball?
  Describe CVE-2010-333.

READY!

🙂 Human: What is the number of files in my current directory multiplied by the number of seasons Babe Ruth played baseball?

System:
You are an assistant is a large language model trained by Google.

Human: TOOLS
------
Assistant can ask the user to use tools to look up information that may be helpful in answering the users original question. The tools the human can use are:

> CVE Database Search: Useful for when you need to look up a CVE Database which catalogs publically disclosed cybersecurity vulnerabilities. The input must be the CVE ID number. For example CVE-2010-3333.
> Google Search: Useful for when you need to answer questions about current events or the current state of the world.
> terminal: Use this tool to run shell command on this MacOS machine or execute gcloud commands for information or tasks related to my google cloud project. The input should always be a string. args {'commands': {'title': 'Commands', 'description': 'List of shell commands to run. Deserialized using json.loads', 'anyOf': [{'type': 'string'}, {'type': 'array', 'items': {'type': 'string'}}]}}
> Calculator: Useful for when you need to answer questions about math

RESPONSE FORMAT INSTRUCTIONS
----------------------------

When responding to me, please output a response in one of two formats:

**Option 1:**
Use this if you want the human to use a tool.
Markdown code snippet formatted in the following schema:

\```json
{
    "action": string, \ The action to take. Must be one of CVE Database Search, Google Search, terminal, Calculator
    "action_input": string \ The input to the action
}
\```

**Option #2:**
Use this if you want to respond directly to the human. Markdown code snippet formatted in the following schema:

\```json
{
    "action": "Final Answer",
    "action_input": string \ You should put what you want to return to use here
}
\```

USER'S INPUT
--------------------
Here is the user's input (remember to respond with a markdown code snippet of a json blob with a single action, and NOTHING else):

What is the number of files in my current directory multiplied by the number of seasons Babe Ruth played baseball?


\```json
{
    "action": "terminal",
    "action_input": "ls | wc -l"
}
\```

/Users/christopher.le/code/iris-code-samples/langchain_with_tools/.venv/lib/python3.11/site-packages/langchain/tools/shell/tool.py:33: UserWarning: The shell tool has no safeguards by default. Use at your own risk.
  warnings.warn(

System:
You are an assistant is a large language model trained by Google.

Human: TOOLS
------
Assistant can ask the user to use tools to look up information that may be helpful in answering the users original question. The tools the human can use are:

> CVE Database Search: Useful for when you need to look up a CVE Database which catalogs publically disclosed cybersecurity vulnerabilities. The input must be the CVE ID number. For example CVE-2010-3333.
> Google Search: Useful for when you need to answer questions about current events or the current state of the world.
> terminal: Use this tool to run shell command on this MacOS machine or execute gcloud commands for information or tasks related to my google cloud project. The input should always be a string. args {'commands': {'title': 'Commands', 'description': 'List of shell commands to run. Deserialized using json.loads', 'anyOf': [{'type': 'string'}, {'type': 'array', 'items': {'type': 'string'}}]}}
> Calculator: Useful for when you need to answer questions about math

RESPONSE FORMAT INSTRUCTIONS
----------------------------

When responding to me, please output a response in one of two formats:

**Option 1:**
Use this if you want the human to use a tool.
Markdown code snippet formatted in the following schema:

\```json
{
    "action": string, \ The action to take. Must be one of CVE Database Search, Google Search, terminal, Calculator
    "action_input": string \ The input to the action
}
\```

**Option #2:**
Use this if you want to respond directly to the human. Markdown code snippet formatted in the following schema:

\```json
{
    "action": "Final Answer",
    "action_input": string \ You should put what you want to return to use here
}
\```

USER'S INPUT
--------------------
Here is the user's input (remember to respond with a markdown code snippet of a json blob with a single action, and NOTHING else):

What is the number of files in my current directory multiplied by the number of seasons Babe Ruth played baseball?
AI: \```json
{
    "action": "terminal",
    "action_input": "ls | wc -l"
}
\```
Human: TOOL RESPONSE:
---------------------
       7


USER'S INPUT
--------------------

Okay, so what is the response to my last comment? If using information obtained from the tools you must mention it explicitly without mentioning the tool names - I have forgotten all TOOL RESPONSES! Remember to respond with a markdown code snippet of a json blob with a single action, and NOTHING else.


\```json
{
    "action": "Final Answer",
    "action_input": "There are 7 files in your current directory and Babe Ruth played baseball for 22 seasons. So the answer is 7 * 22 = 154."
}
\```

🤖 AI: There are 7 files in your current directory and Babe Ruth played baseball for 22 seasons. So the answer is 7 * 22 = 154.
🙂 Human: !q


```