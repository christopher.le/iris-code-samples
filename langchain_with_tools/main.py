from colorama import Fore, Style
from langchain.agents import AgentType
from langchain.agents import initialize_agent
from langchain.memory import ConversationBufferMemory
from llm.vertex_ai import google_bard
from prompt_toolkit import PromptSession
from prompt_toolkit.history import FileHistory
from tools import calculator
from tools import cve_search
from tools import google_search
from tools import shell_tool
import google.auth


# Print out help and some examples.
HELP = f"""
{Style.BRIGHT}{Fore.LIGHTWHITE_EX}
LANGCHAIN WITH TOOLS EXAMPLE
----------------------------
{Style.RESET_ALL}
{Fore.LIGHTGREEN_EX}How it works:{Style.RESET_ALL}
  - {Fore.LIGHTBLUE_EX}This color is what is sent to Vertex AI as the prompt.{Style.RESET_ALL}
  - {Fore.LIGHTYELLOW_EX}This color is what is raw output of Vertex AI.{Style.RESET_ALL}

{Fore.LIGHTGREEN_EX}Instructions:{Style.RESET_ALL}
  - Up/down: Scroll through your previous history.
  - Left/right/delete/backspace: Edit your input.
  - Type "!Q" or Control-C to quit this program.

{Fore.LIGHTGREEN_EX}Try these:{Style.RESET_ALL}
  What is the current new about the Ukraine war?
  What files are in my current directory?
  What is the number of files in my current directory multiplied by the number of seasons Babe Ruth played baseball?
  Describe CVE-2010-333.

READY!
"""

# System prompt template.
PROMPT_TEMPLATE = """
You are an assistant is a large language model trained by Google.
"""


def ask_chatbot(user_input: str):
    """Starts the chatbot chain with the user input.

    Args:
        user_input: User's input

    Returns:
        The final answer after the langchain loop.
    """

    # Setup the memory for when it talks to itself.
    memory = ConversationBufferMemory(
        memory_key='chat_history',
        return_messages=True
    )

    # Tools that the LLM can use.
    llm_tools = [
        cve_search(),       # Search CVE advisories
        google_search(),    # Search Google.
        shell_tool(),       # Use your shell
        calculator()        # Perform basic calculations
    ]

    # Use Google text-bison model.
    llm = google_bard()

    agent_chain = initialize_agent(
        llm_tools,
        llm,
        memory=memory,
        # If the LLM returns a bad response, tell it to try again.
        handle_parsing_errors="Check your output and make sure it conforms!",
        agent=AgentType.CHAT_CONVERSATIONAL_REACT_DESCRIPTION,
        agent_kwargs={
            'system_message': PROMPT_TEMPLATE
        }
    )

    return agent_chain.run(input=user_input)


def main():

    print('\nGetting Google credentials. Please wait ...')
    credentials, _ = google.auth.default()
    print(f'Client ID = {credentials.client_id}')
    print(HELP)

    # "Prompt toolkit" is just for the CLI input.
    console_session = PromptSession(history=FileHistory('console_session.txt'))

    # Until the user types "!q" for quit, loop forever.
    done = False
    while not done:
        user_input = console_session.prompt(f'🙂 Human: ')
        if len(user_input) > 0:
            if user_input.upper() == '!Q':
                done = True
            else:
                response = ask_chatbot(user_input=user_input)
                print(f'🤖 AI: {response}')


if __name__ == '__main__':
    main()
