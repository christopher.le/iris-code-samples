"""
Shell tool provides the LLM with access to execute commands in your terminal.

Be very careful. For example, don't tell it to "delete all my files" because
it will!!
"""

from langchain.callbacks.manager import CallbackManagerForToolRun
from langchain.tools import ShellTool
from typing import List
import platform


class CustomShellTool(ShellTool):
    """Monkey patch the Shell tool to fix bug where command ends with a double quote is missing."""
    def _run(self, commands: str | List[str], run_manager: CallbackManagerForToolRun | None = None) -> str:

        if isinstance(commands, str):
            quote_count = commands.count('"')
            if quote_count %2 == 1: # odd number of quotes
                commands = commands + '"'

        return self.process.run(commands)

def shell_tool():
    if platform.system() == "Darwin":
        my_platform = "MacOS"
    else:
        my_platform = platform.system()
    tool = CustomShellTool()

    tool.description = (
        f"Use this tool to run shell command on this {my_platform} machine or "
        + "execute gcloud commands for information or tasks related to my "
        + "google cloud project. The input should always be a string. " \
        + f"args {tool.args}".replace("{", "{{").replace("}", "}}")
    )
    return tool
