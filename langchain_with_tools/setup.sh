#!/bin/bash

# Install dependencies used across all examples.
source ../bin/global_setup.sh

# Install dependencies specific to this example.
pip install langchain
pip install requests
