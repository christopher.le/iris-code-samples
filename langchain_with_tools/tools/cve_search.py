"""
CVE Search searches the public CVE database.
"""

from langchain.tools import BaseTool
from typing import Any
import requests


class CVESearch(BaseTool):


    name = "CVE Database Search"
    description = (
        "Useful for when you need to look up a CVE Database which "
        "catalogs publically disclosed cybersecurity vulnerabilities. "
        "The input must be the CVE ID number. For example CVE-2010-3333."
    )

    def _run(self, cve_id: str, **kwargs: Any) -> Any:
        """Search the public CVE database.

        Args:
            cve_id: CVE ID as a string (eg: CVE-2010-3333)

        Returns:
            String containing of the summary solutions from the advisory.
        """

        result = requests.get(f"https://cve.circl.lu/api/cve/{cve_id}")
        data = result.json()

        summaries = ""
        for capec in data["capec"]:
            summaries += (
                f"ID: {capec['id']}\n" +
                f"Name: {capec['name']}\n" +
                f"Summary: {capec['summary']}\n" +
                f"Solutions: {capec['solutions']}\n"
            )

        return (
            f"Published: {data['Published']}\n" +
            f"Summary: {data['summary']}\n"
            f"\n{summaries}"
        )

    def _arun(self, cve_id: str, **kwargs: Any):
        raise NotImplementedError("CVESearch does not support async")


def cve_search():
    return CVESearch()