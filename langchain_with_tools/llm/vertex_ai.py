"""
Customized version of langchain's VertexAI.

This customized class also prints out the input and output prompts so we can
see what's going on behind the scenes.
"""

from colorama import Fore, Style
from langchain.callbacks.manager import CallbackManagerForLLMRun
from langchain.llms import VertexAI
from typing import Any, List
import google.auth

class CustomizedVertexAI(VertexAI):
    """Monkey patch langchain's VertexAI class so we can see the prompt."""

    def _call(self,
              prompt: str,
              stop: List[str] | None = None,
              run_manager: CallbackManagerForLLMRun | None = None,
              **kwargs: Any) -> str:
        print(f"\n{Fore.LIGHTBLUE_EX}{prompt}{Style.RESET_ALL}\n")
        result = self._predict(prompt, stop, **kwargs)
        print(f"\n{Fore.LIGHTYELLOW_EX}{result}{Style.RESET_ALL}\n")
        return result

def google_bard():
    credentials, _ = google.auth.default()

    return CustomizedVertexAI(
        location='us-central1',
        project='acn-agbg-ai',
        credentials=credentials,

        temperature=0,
        max_output_tokens=1024,
        top_p=0.95,
        top_k=40,

        verbose=True
    )